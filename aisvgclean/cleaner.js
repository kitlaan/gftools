var version = "1.0.0";

var dropzone = document.getElementById('dropzone');
var loader = document.getElementById('file');
var information = document.getElementById('information');

var prefs = {
    'layercolor': document.getElementById('layercolor'),
};

if (typeof window.FileReader == 'undefined') {
    dropzone.innerText = 'File API not available';
}

dropzone.ondragover = function() {
    this.className = 'hover';
    return false;
};

dropzone.ondragend = function() {
    this.className = '';
    return false;
};

dropzone.ondrop = function(e) {
    this.className= '';
    e.preventDefault();

    loadFile(e.dataTransfer.files[0]);

    return false;
};

loader.onchange = function() {
    loadFile(this.files[0]);
};

document.addEventListener('copy', function(e) {
    if (dropzone.svgout) {
        e.clipboardData.setData('text/plain', dropzone.svgout);
        e.clipboardData.setData('image/svg+xml', dropzone.svgout);
        e.preventDefault();
    }
});

document.addEventListener('paste', function(e) {
    if (e.clipboardData.types.indexOf('image/svg+xml') > -1) {
        convertSVG('clipboard.svg', e.clipboardData.getData('image/svg+xml'));
        e.preventDefault();
    }
    else if (e.clipboardData.types.indexOf('text/plain') > -1) {
        convertSVG('clipboard.svg', e.clipboardData.getData('text/plain'));
        e.preventDefault();
    }
});

///////////////////////////////////////////////////////////

function loadFile(filename) {
    infoClear();

    var reader = new FileReader();
    reader.onload = function(e) {
        convertSVG(filename.name, e.target.result);
    };
    reader.onerror = function(e) {
        infoLog('could not load ' + filename.name);
    }
    reader.readAsText(filename, 'utf-8');
}

function convertSVG(filename, text) {
    // https://community.glowforge.com/t/laser-design-basics/7408

    var parser = new DOMParser();
    var xml = parser.parseFromString(text, "text/xml");

    // save it for help debugging...
    dropzone.xml = xml;

    var changed = false;

    // insert a comment so we know it's been processed
    var comment = window.location + '   Version: ' + version +
                  '   Date: ' + (new Date()).toISOString();
    xml.insertBefore(document.createComment(comment), xml.firstChild);

    // make sure 'svg'; if not, fail out
    if (xml.rootElement.tagName != 'svg') {
        infoLog('not a SVG file');
        return false;
    }

    // fix up width/height attributes
    var viewbox = xml.rootElement.getAttribute('viewBox');
    if (viewbox) {
        viewbox = viewbox.split(/\s+/);
        if (viewbox.length != 4) {
            infoLog('wrong number of viewBox items: ' + viewbox.length);
            return false;
        }

        var width = xml.rootElement.getAttribute('width');
        var height = xml.rootElement.getAttribute('height');

        // if no width/height, use the value from viewBox
        // if have width/height, but not 'pt' format, fix (and verify same as viewBox)

        if (!width || width.startsWith(viewbox[2])) {
            if (width != viewbox[2] + 'pt') {
                xml.rootElement.setAttribute('width', viewbox[2] + 'pt');
                infoLog('fixing width attribute');
                changed = true;
            }
        }
        else if (width.endsWith('in') || width.endsWith('mm')) {
            infoLog('width is already in absolute measurements: ' + width);
        }
        else {
            infoLog('width mismatch: ' + width + ' / ' + viewbox[2]);
            return false;
        }

        if (!height || height.startsWith(viewbox[3])) {
            if (height != viewbox[3] + 'pt') {
                xml.rootElement.setAttribute('height', viewbox[3] + 'pt');
                infoLog('fixing height attribute');
                changed = true;
            }
        }
        else if (height.endsWith('in') || height.endsWith('mm')) {
            infoLog('height is already in absolute measurements: ' + height);
        }
        else {
            infoLog('height mismatch: ' + height + ' / ' + viewbox[3]);
            return false;
        }
    }
    else {
        infoLog('no viewBox');
        return false;
    }

    // detect "raster" images embedded, warn about it
    var numImageNonEmbed = 0;
    for (var e of xml.rootElement.getElementsByTagName('image')) {
        var href = e.getAttribute('xlink:href');
        if (href && !href.startsWith('data:')) {
            numImageNonEmbed++;
        }
    }
    if (numImageNonEmbed > 0) {
        infoLog('found ' + numImageNonEmbed + ' non-embedded image element(s)');
    }

    if (xml.rootElement.getElementsByTagName('text').length > 0 ||
            xml.rootElement.getElementsByTagName('textPath').length > 0) {
        infoLog('text element(s) found');
    }

    // delete 'pattern' element(s), since we won't be using them...
    var numPatternFound = 0;
    for (var e of xml.rootElement.getElementsByTagName('pattern')) {
        numPatternFound++;
        e.parentElement.removeChild(e);
        changed = true;
    }
    if (numPatternFound > 0) {
        infoLog('removed ' + numPatternFound + ' pattern element(s)');
    }

    if (prefs.layercolor.checked) {
        // each element... generate fill/stroke attribute sequentially
        // fill="none" and stroke="none" -- ignored
        // fill!="none" -- engrave
        // stroke!="none" -- cut/score
        var numLayersFound = 0;
        var numIgnored = 0;
        var color = 0;
        for (var e of xml.rootElement.children) {
            numLayersFound++;

            var fs = getFillStroke(e);
            if (fs) {
                // SVG default fill is black
                if (fs.fill == 'none') {
                    e.setAttribute('fill', 'none');
                }
                else {
                    e.setAttribute('fill', decToHex(color++));
                }

                if (!fs.stroke || fs.stroke == 'none') {
                    e.setAttribute('stroke', 'none');
                }
                else {
                    e.setAttribute('stroke', decToHex(color++));
                }
            }
            else {
                numIgnored++;
            }
        }
        infoLog('found ' + numLayersFound + ' layers (' + numIgnored + ' ignored)');
        changed = true;
    }

    // write xml out to file
    if (changed) {
        infoLog('generating new file...');
        exportSVG(filename, xml);
    }
    else {
        infoLog('no changes');
    }

    return true;
}

function exportSVG(filename, xml) {
    var serializer = new XMLSerializer();
    var output = serializer.serializeToString(xml);

    dropzone.svgout = output;
    var url = 'data:image/svg+xml;utf8,' + unescape(encodeURIComponent(output));

    var link = document.createElement('a');
    link.download = filename;
    link.href = url;
    link.click();
}

function getFillStroke(e) {
    var ret = {
        'fill': e.getAttribute('fill'),
        'stroke': e.getAttribute('stroke'),
    };

    e.removeAttribute('fill');
    e.removeAttribute('stroke');

    for (var c of e.children) {
        var fs = getFillStroke(c);

        if (!fs) {
            return null;
        }
        else if (fs.fill && ret.fill && (fs.fill != ret.fill)) {
            return null;
        }
        else if (fs.stroke && ret.stroke && (fs.stroke != ret.stroke)) {
            return null;
        }

        if (fs.fill && !ret.fill) {
            ret.fill = fs.fill;
        }
        if (fs.stroke && !ret.stroke) {
            ret.stroke = fs.stroke;
        }
    }

    return ret;
}

///////////////////////////////////////////////////////////

function infoClear() {
    while (information.hasChildNodes()) {
        information.removeChild(information.lastChild);
    }
}

function infoLog(s) {
    information.appendChild(document.createTextNode(s));
    information.appendChild(document.createElement('br'));
}

///////////////////////////////////////////////////////////

function hex(x) {
    var hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function decToHex(d) {
    var b = d % 256;
    var g = ((d - b) / 256) % 256;
    var r = ((d - b - (g * 256)) / (256 * 256)) % 256;
    return "#" + hex(r) + hex(g) + hex(b);
}
