var trayObj = document.getElementById('tray');
var objectObj = document.getElementById('object');
var shimObj = document.getElementById('shim');

var objectSize = document.getElementById('objectsize');
var shimSize = document.getElementById('shimsize');
var gfSize = document.getElementById('gfsize');
var shimMsg = document.getElementById('shimstatement');
var shimField = document.getElementById('shimfield');
var gfField = document.getElementById('gffield');

var gfTray = 1.4;
var gfMin = 0.01;
var gfMax = 0.43;


/////////////////////////////////////////////////////////////////////


// NOTE: doing all decimal math scaled up by 2-decimals, to avoid damn
// floating point annoyances.

function rescale(f) {
    return parseInt(Math.floor(f * 100));
}

gfTray = rescale(gfTray);
gfMin = rescale(gfMin);
gfMax = rescale(gfMax);


/////////////////////////////////////////////////////////////////////


objectSize.oninput = shimSize.oninput = function() {
    if (this.validity.valid) {
        recompute();
    }
};


/////////////////////////////////////////////////////////////////////


// https://community.glowforge.com/t/tray-removed-calculator/10927

function recompute() {
    if (!objectSize.value) {
        shimMsg.innerText = '';
        shimField.style.visibility = 'hidden';
        gfField.style.visibility = 'hidden';

        showObj(50, 0, true);
        return;
    }

    var osz = rescale(parseFloat(objectSize.value));
    var objHeight = translate(osz, 0, (gfMax + gfTray), 0, 317 + 94 + 3);

    if (osz < gfMin) {
        shimMsg.innerText = 'object too small, need to shim to at least 0.01"';
        shimField.style.visibility = 'hidden';
        gfField.style.visibility = 'hidden';

        showObj(2, 0, true);
        return;
    }

    if (osz <= gfMax) {
        shimMsg.innerText = '';
        shimField.style.visibility = 'hidden';
        gfField.style.visibility = 'visible';

        showObj(objHeight, 0, true);
        gfSize.value = objectSize.value;
        return;
    }

    shimField.style.visibility = 'visible';

    if (!shimSize.value) {
        shimMsg.innerText = 'object too big, remove tray and insert shim';
        gfField.style.visibility = 'hidden';

        showObj(objHeight, 0, false);
        return;
    }

    var ssz = rescale(parseFloat(shimSize.value));
    var shimHeight = translate(ssz, 0, (gfMax + gfTray), 0, 317 + 94 + 3);

    if ((ssz + osz) < (gfMin + gfTray)) {
        shimMsg.innerText = 'shim needs to be bigger';
        gfField.style.visibility = 'hidden';

        showObj(objHeight, shimHeight, false);
        return;
    }

    if ((ssz + osz) > (gfMax + gfTray)) {
        shimMsg.innerText = 'shim needs to be smaller';
        gfField.style.visibility = 'hidden';

        showObj(objHeight, 0, false);
        return;
    }

    else {
        shimMsg.innerText = '';
        gfField.style.visibility = 'visible';

        showObj(objHeight, shimHeight, false);
        gfSize.value = ((ssz + osz) - gfTray) / 100;
        return;
    }
}

/////////////////////////////////////////////////////////////////////

function showObj(o, s, t) {
    trayObj.style.visibility = t ? 'visible' : 'hidden';
    shimObj.style.visibility = s ? 'visible' : 'hidden';

    objectObj.style.height = o + 'px';
    shimObj.style.height = s + 'px';

    var bottom = 15;
    if (trayObj.style.visibility == 'visible') {
        trayObj.style.bottom = bottom + 'px';
        bottom += trayObj.getBoundingClientRect().height;
    }
    if (shimObj.style.visibility == 'visible') {
        shimObj.style.bottom = bottom + 'px';
        bottom += shimObj.getBoundingClientRect().height;
    }
    objectObj.style.bottom = bottom + 'px';
}

function translate(v, inMin, inMax, outMin, outMax) {
    if (v <= inMin) {
        return outMin;
    }
    else if (v >= inMax) {
        return outMax;
    }
    else {
        var ratio = (v - inMin) / (inMax - inMin);
        return parseInt(Math.round(ratio * (outMax - outMin))) + outMin;
    }
}

recompute();
